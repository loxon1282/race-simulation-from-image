﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    public bool check= false;
    private void OnTriggerEnter2D(Collider2D other) {
        if(!check){
            GameManager.instance.Race.checkCount++;
            check = true;
        }    
    }
}
