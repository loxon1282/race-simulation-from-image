﻿using UnityEngine;
using System.Collections;

public class Car2dController : MonoBehaviour {

	
	public float speedForce = 15f;
	public float torqueForce = -200f;
	public float driftFactorSticky = 0.2f;
	public float driftFactorSlippy = 1;
	public float maxStickyVelocity = 0.5f;
	public float minSlippyVelocity = 1.5f;	
	public bool driftMode = false;

	[Range(1,5)]
	public float breaksForce = 2.0f;
	Rigidbody2D rb;
	void Start()
	{
		rb = GetComponent<Rigidbody2D>();
	}
	void FixedUpdate () {

		float driftFactor = driftFactorSticky;
		if(RightVelocity().magnitude > maxStickyVelocity) {
			driftFactor = driftFactorSlippy;
		}
		rb.velocity = ForwardVelocity() + RightVelocity()*driftFactor;

		if(Input.GetButton("Horizontal")) {
			rb.AddForce( transform.up * speedForce*Input.GetAxis("Horizontal") );
		}
		if(Input.GetButton("Vertical")) {
			rb.AddForce( transform.up * -speedForce/2f*Input.GetAxis("Vertical") );

		}
		if(Input.GetKey(KeyCode.Space)){
			rb.AddForce(-rb.velocity*breaksForce);
		}
		float tf = Mathf.Lerp(0, torqueForce, rb.velocity.magnitude / 2);
		rb.angularVelocity = Input.GetAxis("Horizontal") * tf;
	}
	Vector2 ForwardVelocity() {
		return transform.up * Vector2.Dot( GetComponent<Rigidbody2D>().velocity, transform.up );
	}

	Vector2 RightVelocity() {
		if(Input.GetAxis("Horizontal")>0&&driftMode)
		return Input.GetAxis("Horizontal")*transform.right * Vector2.Dot( GetComponent<Rigidbody2D>().velocity, transform.right );
		else if(Input.GetAxis("Horizontal")<0&&driftMode)
		return Input.GetAxis("Horizontal")*-transform.right * Vector2.Dot( GetComponent<Rigidbody2D>().velocity, transform.right );
		
		return Vector2.zero;
	}


}
