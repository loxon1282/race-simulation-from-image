﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Track",menuName = "Track")]
public class Track : ScriptableObject
{
    public Texture2D Map;
    public Colors colors;
    public Vector2 startPosition;
    public float PlayerRotationAngle, RiverForceAngle;
    public List<Vector3> checkpointsPosition;
    
}
[System.Serializable]
public class Colors{
    public Color trackEndColor,riverColor=new Color32(33,157,226,255),bridgeColor;
}
