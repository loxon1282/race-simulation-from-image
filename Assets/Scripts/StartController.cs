﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartController : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other) {
            GameManager.instance.Race.NewLap();
    }
}
