﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class RaceBehavior : MonoBehaviour
{
    public Color trackColor = new Color(0,0,0,1f),riverColor = new Color(0,0,0,1f),bridgeColor = new Color(0,0,0,1f);
    public Texture2D track;
    public Transform car;
    public Rigidbody2D RBCar;
    Vector2 Carposition;
    public float lapTime = 0f;
    public TextMeshProUGUI Timer,Best;
    public Transform StartTr;
    int checkpoints;
    public float riverForce = 4f;
    public int checkCount;
    
    void Update()
    {
        lapTime+=Time.deltaTime;
        Carposition = (car.transform.position);
        if(CheckColor(trackColor))
        {
            Debug.Log("Out of track");
            SlowDown();
        }
        else if(CheckColor(riverColor))
        {
            Debug.Log("River");
            AddRiverForce();
        }
        else if(CheckColor(bridgeColor)){
            Debug.Log("Bridge");
            AddBridgeForece();
        }
        Timer.text="Time: "+lapTime.ToString("F2");

    }
    public void SlowDown()
    {
        RBCar.AddForce(-RBCar.velocity*2f);
    }
    public void AddRiverForce()
    {
        RBCar.AddForce(-RBCar.velocity*0.3f);
        float angle = GameManager.instance.Tracks[GameManager.instance.TrackIndex].RiverForceAngle-90;
        float turn;

        if(angle>=0&&angle<180)
            turn=-1f;
        else 
            turn = 1f;

        float xF = Mathf.Cos(Mathf.Deg2Rad*angle)*riverForce/15f*turn;

        if(angle>=90&&angle<270)
            turn=-1f;
        else 
            turn = 1f;

        float yF = Mathf.Sin(Mathf.Deg2Rad*angle)*riverForce/15f*turn; 

        Transform t = RBCar.gameObject.transform;
        t.position= new Vector2(RBCar.gameObject.transform.position.x+xF,RBCar.gameObject.transform.position.y+yF);

        Debug.Log("riv force: "+new Vector2(xF,yF));

    }
    public void AddBridgeForece()
    {
        RBCar.AddForce(-RBCar.velocity*20f);
    }
    public bool CheckColor(Color color)
    {
        if(track.GetPixel((int)(Carposition.x-.5f),(int)(Carposition.y-.5f)).r-(color.r)==0&&
        track.GetPixel((int)(Carposition.x-.5f),(int)(Carposition.y-.5f)).g-(color.g)==0&&
        track.GetPixel((int)(Carposition.x-.5f),(int)(Carposition.y-.5f)).b-color.b==0)
        {
            return true;
        }
        return false;
    }
    public void NewLap(){
        
        int trackInd=GameManager.instance.TrackIndex;
        
        if(PlayerPrefs.GetFloat(string.Join("BestLap",trackInd.ToString()),999f)>lapTime&&lapTime>3f){
        foreach (GameObject checkpoint in GameObject.FindGameObjectsWithTag("Checkpoint"))
        {
         if(!checkpoint.GetComponent<Checkpoint>().check)  
                return; 
         
        }
        foreach (GameObject checkpoint in GameObject.FindGameObjectsWithTag("Checkpoint"))
        {
         checkpoint.GetComponent<Checkpoint>().check= false;
         
        }
             PlayerPrefs.SetFloat(string.Join("BestLap",trackInd.ToString()),lapTime);   
        }
        lapTime=0;
        Best.text = "Best: "+PlayerPrefs.GetFloat(string.Join("BestLap",trackInd.ToString()),999f).ToString("F2");
    }
}
