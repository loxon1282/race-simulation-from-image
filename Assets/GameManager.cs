﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public RaceBehavior Race;
    public List<Track> Tracks = new List<Track>();
    private int trackIndex;
    public GameObject CheckpointPrefab;
    public  int TrackIndex
    {
        get
        {
            return trackIndex;
        }
    }

    void Awake() 
    {
        PlayerPrefs.DeleteAll();
        if(instance==null)
            instance = this;
        DontDestroyOnLoad(gameObject);  
        if(Race==null)
        Race=GameObject.FindGameObjectWithTag("Map").GetComponent<RaceBehavior>();  
    }
    void Start()
    {
     ResetLevel();
    }

    void Update()
    {
        
    }
    public void NextTrack(){
        trackIndex++;
        if(trackIndex>=Tracks.Count)
        trackIndex=0;
        
        ResetLevel();

    }
     public void PrevTrack(){
        trackIndex--;
        if(trackIndex<=0)
        trackIndex=Tracks.Count-1;
        
        ResetLevel();

    }
    public void ResetLevel(){
        DestroyCheckpoints();
        Track tr = Tracks[trackIndex];
        Race.car.rotation = Quaternion.Euler(Race.car.rotation.x,Race.car.rotation.y,tr.PlayerRotationAngle);
        Race.car.position = tr.startPosition;
        Race.trackColor = tr.colors.trackEndColor;
        Race.riverColor = tr.colors.riverColor;
        Race.bridgeColor = tr.colors.bridgeColor;
        Race.StartTr.position= tr.startPosition;
        GenerateCheckpoints();
        Race.track = tr.Map;
        Race.GetComponent<Renderer>().material.mainTexture= tr.Map;
        Race.NewLap();
        Race.lapTime = 0;
    }
    private void DestroyCheckpoints(){
        foreach(GameObject chp in GameObject.FindGameObjectsWithTag("Checkpoint")){
            Destroy(chp);
        }
    }
    private void GenerateCheckpoints(){
        foreach(Vector2 pos in Tracks[trackIndex].checkpointsPosition){
            GameObject tmp = Instantiate(CheckpointPrefab);
            tmp.transform.position = pos; 
        }
    }
}
